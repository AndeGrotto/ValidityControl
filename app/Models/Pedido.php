<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;
    protected $table = "pedidos";
    protected $fillable = ['dataPedido', 'valorTotal', 'cliente_id'];

    public function produtos() {
        return $this->hasMany("App\Models\ItensPedido");
    }
    
    public function cliente(){
        return $this->belongsTo("App\Models\Cliente");
    }
}
