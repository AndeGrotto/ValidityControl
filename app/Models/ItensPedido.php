<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItensPedido extends Model
{
    use HasFactory;
    protected $table = "itensPedidos";
    protected $fillable = ['quantidadeItem', 'valorTotalItem', 'pedido_id', 'produto_id'];

    public function produto(){
        return $this->belongsTo("App\Models\Produto");
    }

    public function pedido(){
        return $this->belongsTo("App\Models\Pedido");
    }
}
