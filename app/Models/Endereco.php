<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    use HasFactory;
    protected $table = "enderecos";
    protected $fillable = ['cep', 'cidade','logradouro', 'numero', 'estado',  'bairro'];

    public function entregas() {
        return $this->hasMany("App\Models\Entrega");
    }

    public function clientes() {
        return $this->hasMany("App\Models\Cliente");
    }

}
