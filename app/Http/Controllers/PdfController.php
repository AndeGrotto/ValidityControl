<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motorista;
use App\Models\Caminhao;
use App\Models\Cliente;
use App\Models\Endereco;
use App\Models\Entrega;
use App\Models\ItensPedido;
use App\Models\Lote;
use App\Models\Pedido;
use App\Models\Produto;
use App\Models\User;
use PDF;

class PdfController extends Controller
{

    public function getAllMotoristas()
    {
        $motoristas = Motorista::all();
        return view('motorista', compact('motoristas'));
    }

    public function getAllCaminhoes()
    {
        $caminhoes = Caminhao::all();
        return view('caminhao', compact('caminhoes'));
    }

    public function getAllClientes()
    {
        $clientes = Cliente::all();
        return view('cliente', compact('clientes'));
    }

    public function getAllEntregas()
    {
        $entregas = Entrega::all();
        return view('entrega', compact('entregas'));
    }

    public function getAllEnderecos()
    {
        $enderecos = Endereco::all();
        return view('endereco', compact('enderecos'));
    }

    public function getAllPedidos()
    {
        $pedidos = Pedido::all();
        return view('pedido', compact('pedidos'));
    }

    //------------------------------------------------------------------------------------
    //Donwload PDF

    public function exportCaminhoesPDF()
    {
        $caminhoes = Caminhao::all();
        $pdf = PDF::loadView('caminhao', compact('caminhoes'));
        return $pdf->download('caminhoes.pdf');
    }

    public function exportClientesPDF()
    {
        $clientes = Cliente::all();
        $pdf = PDF::loadView('cliente', compact('clientes'));
        return $pdf->download('clientes.pdf');
    }

    public function exportEnderecosPDF()
    {
        $enderecos = Endereco::all();
        $pdf = PDF::loadView('endereco', compact('enderecos'));
        return $pdf->download('enderecos.pdf');
    }

    public function exportEntregasPDF()
    {
        $entregas = Entrega::all();
        $pdf = PDF::loadView('entrega', compact('entregas'));
        return $pdf->download('entregas.pdf');
    }

    public function exportItensPedidosPDF()
    {
        $itensPedidos = ItensPedido::all();
        $pdf = PDF::loadView('itensPedido', compact('itensPedidos'));
        return $pdf->download('itensPedidos.pdf');
    }

    public function exportLotesPDF()
    {
        $lotes = Lote::all();
        $pdf = PDF::loadView('lote', compact('lotes'));
        return $pdf->download('lotes.pdf');
    }

    public function exportMotoristasPDF()
    {
        $motoristas = Motorista::all();
        $pdf = PDF::loadView('motorista', compact('motoristas'));
        return $pdf->download('motoristas.pdf');
    }


    public function exportPedidosPDF()
    {
        $pedidos = Pedido::all();
        $pdf = PDF::loadView('pedido', compact('pedidos'));
        return $pdf->download('pedidos.pdf');
    }

    public function exportProdutosPDF()
    {
        $produtos = Produto::all();
        $pdf = PDF::loadView('produto', compact('produtos'));
        return $pdf->download('produtos.pdf');
    }

    public function exportUsersPDF()
    {
        $users = User::all();
        $pdf = PDF::loadView('user', compact('users'));
        return $pdf->download('users.pdf');
    }
}
