<?php

namespace App\Http\Controllers;

use App\Exports\EnderecosExport;
use App\Http\Requests\ApiRequest;
use App\Http\Requests\EnderecoRequest;
use App\Models\Endereco;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class EnderecosController extends Controller
{

    public function list(Request $filtro)
    {
        $filtragem = $filtro->get('desc_filtro');
        if ($filtragem == null)
            $enderecos = Endereco::orderBy('id')->paginate(5);
        else
            $enderecos = Endereco::where('cidade', 'like', '%' . $filtragem . '%')
                ->orderBy("id")
                ->paginate(5)
                ->setpath('enderecos?desc_filtro=' . $filtragem);

        return view('enderecos.list', ['enderecos' => $enderecos]);
    }

    public function create()
    {
        return view('enderecos.create');
    }

    public function search()
    {
        return view('enderecos.search');
    }

    public function search_cep(ApiRequest $request)
    {
        try
        {
            $cep = str_replace("-", "", $request->input('cep'));
            $response = Http::get("viacep.com.br/ws/$cep/json/")->json();
            //dd($response['erro']);
            return view('enderecos.create')->with(
                [
                    'cep' => $request->input('cep'),
                    'logradouro' => $response['logradouro'],
                    'bairro' => $response['bairro'],
                    'cidade' => $response['localidade'],
                    'estado' => $response['uf']
                ]
                );
        } catch (Throwable $e) {
            return redirect()->route('enderecos.search')->withErro('CEP ' . $request->input('cep') . ' não foi encontrado!');
        }



    }

    public function store(EnderecoRequest $request)
    {
        /*$cep = Endereco::where('cep', $request->input('cep'))->first();*/
        $numero = Endereco::where('numero', $request->input('numero'))->first();
        $novo_endereco = $request->all();
        if (!$numero) {
            Endereco::create($novo_endereco);
            return redirect()->route('enderecos')->withSucesso('O endereço foi salvo com sucesso!');
        } /*else if (!$numero && !$cep) {
            Endereco::create($novo_endereco);
            return redirect()->route('enderecos')->withSucesso('O endereço foi salvo com sucesso!');
        }*/ else {
            return redirect()->route('enderecos.search')->withErro('O endereço já está cadastrado.');
        }
    }


    public function destroy($id)
    {
        try {
            Endereco::find($id)->delete();
            $ret = array('status' => 200, 'msg' => "null");
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status' => 500, 'msg' => $e->getMessage());
        } catch (\PDOException $e) {
            $ret = array('status' => 500, 'msg' => $e->getMessage());
        }
        return $ret;
    }
    public function edit($id)
    {
        $endereco = Endereco::find($id);
        return view('enderecos.edit', compact('endereco'));
        dd($endereco);
    }

    public function update(EnderecoRequest $request, $id)
    {
        //dd($id);
        Endereco::find($id)->update($request->all());
        return redirect()->route('enderecos');
    }

    public function export()
    {
        return Excel::download(new EnderecosExport, 'enderecos.xlsx');
        return redirect()->route('enderecos');
    }
}
