<?php

namespace App\Http\Controllers;

use App\Exports\PedidosExport;
use App\Http\Requests\PedidoRequest;
use App\Models\ItensPedido;
use App\Models\Pedido;
use App\Models\Produto;
use Brick\Math\BigDecimal;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PedidosController extends Controller
{
    public function list(Request $filtro)
    {
        $filtragem = $filtro->get('desc_filtro');
        if ($filtragem == null)
            $pedidos = Pedido::orderBy('id')->paginate(5);
        else
            $pedidos = Pedido::where('dataPedido', 'like', '%' . $filtragem . '%')
                ->orderBy("id")
                ->paginate(5)
                ->setpath('pedidos?desc_filtro=' . $filtragem);

        return view('pedidos.list', ['pedidos' => $pedidos]);
    }

    public function create()
    {
        return view('pedidos.create');
    }

    public function store(PedidoRequest $request)
    {

        $novo_pedido = $request->all();
        $produtos = $request->produtos;
        $valorTotal = 0;


        if ($produtos == null) {
            return redirect()->route('pedidos.create')->withErro('Deve ser adicionado pelo menos um produto');
        } else {
            foreach ($produtos as $p => $value) {
                $produto[$p] = Produto::whereId($novo_pedido["produtos"][$p])->first();

                $quantidadeItem[$p] = intval($novo_pedido["quantidadeItem"][$p]);
                //dd($quantidadeItem[$p]);

                $preçoProduto[$p] = floatval($produto[$p]->preco);
                $valorTotalItem[$p] = $preçoProduto[$p] * $quantidadeItem[$p];
                $valorTotal = $valorTotal + ($preçoProduto[$p] * $quantidadeItem[$p]);

                $estoque = intval($produto[$p]->estoque);
                if ($quantidadeItem[$p] > $estoque) {
                    return redirect()->route('pedidos.create')->withErro($produto[$p]->descricao . ' tem em estoque: ' . $estoque . ' unidades. Insira uma quantidade menor!');
                } elseif ($quantidadeItem[$p] < 0) {
                    return redirect()->route('pedidos.create')->withErro('Quantidade deve ser maior que 0!');
                } elseif ($quantidadeItem[$p] > 99999) {
                    return redirect()->route('pedidos.create')->withErro('Quantidade deve ser menor que 99999!');
                }
            }
            

            $pedido = Pedido::create([
                'descricao' => $request->get('descricao'),
                'dataPedido' => $request->get('dataPedido'),
                'valorTotal' => $valorTotal,
                'cliente_id' => $request->get('cliente_id')
            ]);

            foreach ($produtos as $p => $value) {

                ItensPedido::create([
                    'pedido_id' => $pedido->id,
                    'produto_id' => $produtos[$p],
                    'quantidadeItem' => $quantidadeItem[$p],
                    'valorTotalItem' => $valorTotalItem[$p]
                ]);
            }

            //dd($valorTotal);


            return redirect()->route('pedidos');
        }
    }

    public function destroy($id)
    {
        try {
            Pedido::find($id)->delete();
            $ret = array('status' => 200, 'msg' => "null");
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status' => 500, 'msg' => $e->getMessage());
        } catch (\PDOException $e) {
            $ret = array('status' => 500, 'msg' => $e->getMessage());
        }
        return $ret;
    }

    public function edit($id)
    {
        $pedido = Pedido::find($id);
        return view('pedidos.edit', compact('pedido'));
    }

    public function update(PedidoRequest $request, $id)
    {
        Pedido::find($id)->update($request->all());
        
        return redirect()->route('pedidos');
    }

    public function export()
    {
        return Excel::download(new PedidosExport, 'pedidos.xlsx');
        return redirect()->route('pedidos');
    }
}
