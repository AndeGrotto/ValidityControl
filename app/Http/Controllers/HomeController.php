<?php

namespace App\Http\Controllers;
use App\Models\Caminhao;
use App\Models\Cliente;
use App\Models\Endereco;
use App\Models\Entrega;
use App\Models\ItensPedido;
use App\Models\Lote;
use App\Models\Motorista;
use App\Models\Pedido;
use App\Models\Produto;
use App\Models\User;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $caminhoes = Caminhao::count();
        $clientes= Cliente::count();
        $enderecos = Endereco::count();
        $entregas = Entrega::count();
        $itensPedidos = ItensPedido::count();
        $lotes = Lote::count();
        $motoristas = Motorista::count();
        $pedidos = Pedido::count();
        $produtos = Produto::count();
        $users = User::count();
        return view('home', compact( 'caminhoes','clientes','enderecos','entregas','itensPedidos','lotes',
        'motoristas','pedidos','produtos','users'));

    }
}
