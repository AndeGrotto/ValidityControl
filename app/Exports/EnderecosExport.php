<?php

namespace App\Exports;

use App\Models\Endereco;
use Maatwebsite\Excel\Concerns\FromCollection;

class EnderecosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Endereco::all();
    }

}
