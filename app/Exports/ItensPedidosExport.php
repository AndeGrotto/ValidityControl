<?php

namespace App\Exports;

use App\Models\ItensPedido;
use Maatwebsite\Excel\Concerns\FromCollection;

class ItensPedidosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ItensPedido::all();
    }
}
