<?php

namespace App\Exports;

use App\Models\Entrega;
use Maatwebsite\Excel\Concerns\FromCollection;

class EntregasExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Entrega::all();
    }

}
