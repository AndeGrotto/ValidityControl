<?php

namespace App\Exports;

use App\Models\Motorista;
use Maatwebsite\Excel\Concerns\FromCollection;

class MotoristasExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Motorista::all();
    }

}
