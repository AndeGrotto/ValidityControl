<?php

namespace App\Exports;

use App\Models\Caminhao;
use Maatwebsite\Excel\Concerns\FromCollection;

class CaminhoesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Caminhao::all();
    }
}
