
    $("#telefone").mask("(00) 00000-0000");
    $("#placa").mask("AAA-9A99");
    $("#cep").mask("00000-000");
    $('#quantidadeProduto').mask('0000');
    $('.quantidadeItem').mask('0000');
    $('#numero').mask('000000');
    $('#validade').mask('000');
    $('.date').mask('00/00/0000');

    $('.time').mask('00:00:00');
    //$('.cep').mask('00000-000');
    $('.phone').mask('(00) 00000-0000');
    $('.cpf').mask('000.000.000-00');
    $('.money').mask('000.000.000.000,00');

function mostrarSenha() {
    var tipo = document.getElementById("password");
    if (tipo.type == "password") {
        tipo.type = "text";
    } else {
        tipo.type = "password";
    }
}

var x = document.getElementsByClassName("cpf_cnpj");
for (i = 0; i < x.length; i++) {
    x[i].addEventListener("focus", function (e) {
        document.getElementById(e.target.id).select();
    });
}
for (var i = 0; i < x.length; i++) {
    x[i].addEventListener("keyup", myFunction, false);
}
function myFunction(e) {
    x = "";
    if (e.keyCode == 8 || e.keyCode == 46) {
        return false;
    }
    if (e.keyCode >= 48 || e.keyCode <= 57) {
        var id = e.target.id;
        document.getElementById(id).setAttribute("maxlength", 18);
        var str = e.target.value.replace(/\D/g, "");
        //var str = str.replace(/^0+/, '');
        var len = str.length; //
        if (len == 11) {
            x =
                str.substr(0, 3) +
                "." +
                str.substr(3, 3) +
                "." +
                str.substr(6, 3) +
                "-" +
                str.substr(9, 2); // 510.791.784-49
        } else if (len > 11 && len < 15) {
            x =
                str.substr(0, 2) +
                "." +
                str.substr(2, 3) +
                "." +
                str.substr(5, 3) +
                "/" +
                str.substr(8, 4) +
                "-" +
                str.substr(12, 2); // 04.498.492/0001-67
        } else {
            x = str;
        }
        document.getElementById(id).value = x;
    }
}

const input = document.getElementById("preco");

input.addEventListener("keyup", formatarMoeda);

function formatarMoeda(e) {
    var v = e.target.value.replace(/\D/g, "");

    v = (v / 100).toFixed(2) + "";


    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");

    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");

    e.target.value = v;
}