<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Caminhoes</title>
    <style>
        #caminhao{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #caminhao td,#caminhao th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #caminhao tr:nth-child(even){
            background-color: #c9eeee;
        }
        #caminhao th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="caminhao">
        <thead>
            <tr>
                <th>ID</th>
                <th>Placa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($caminhoes as $caminhao)
                <tr>
                    <td>{{ $caminhao->id }}</td>
                    <td>{{ $caminhao->placa }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>