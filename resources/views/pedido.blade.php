<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pedidos</title>
    <style>
        #pedido{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #pedido td,#pedido th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #pedido tr:nth-child(even){
            background-color: #c9eeee;
        }
        #pedido th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="pedido" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Data do Pedido</th>
                <th>Cliente*</th>
                <th>Produto(s)*</th>
            </tr>

        </thead>
        <tbody>
            @foreach ($pedidos as $pedido)
                <tr>
                    <td>{{ $pedido->id }}</td>
                    <td>{{ Carbon\Carbon::parse($pedido->dataPedido)->format('d/m/Y') }}</td>
                    <td>{{ isset($pedido->cliente->razaoSocial) ? $pedido->cliente->razaoSocial : 'Cliente não informado' }}
                    </td>
                    <td>
                        @foreach($pedido->produtos as $p)
                            <li>{{ ($p->produto->descricao) }}</li>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>