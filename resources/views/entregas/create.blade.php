<link rel="stylesheet" href="/css/style.css">
@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Nova Entrega </h3>
            </div>
            <div class="icon">
                <i class="fa fa-map" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'entregas.store']) !!}

            <div class="form-group">
                {!! Form::label('dataSaida', 'Data de Saída') !!}
                {!! Form::date('dataSaida', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('dataEntrega', 'Data de Entrega:') !!}
                {!! Form::date('dataEntrega', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('motorista_id', 'Motorista:') !!}
                {!! Form::select(
                    'motorista_id',
                    \App\Models\Motorista::orderBy('nome')->pluck('nome', 'id')->toArray(),
                    null,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>
            <div class="form-group">
                {!! Form::label('endereco_id', 'Endereço:') !!}
                {!! Form::select(
                    'endereco_id',
                    \App\Models\Endereco::orderBy('cep')->pluck('cep', 'id')->toArray(),
                    null,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>

            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('entregas', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>   
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" crossorigin="anonymous"></script>
            <script src="/js/scripts.js"></script>

            {!! Form::close() !!}
        </div>
    </div>
@stop
