<link rel="stylesheet" href="/css/style.css">
@extends('layouts.default')

@section('content')

    <h1>Entregas</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'entregas']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}
    <br>

    <a href="{{ route('entregas.exportEntregasPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('entregas.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Data de Saída</th>
            <th>Data de Entrega</th>
            <th>Motorista*</th>
            <th>Endereço*</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($entregas as $entrega)
                <tr>
                    <td>{{ $entrega->id }}</td>
                    <td>{{ Carbon\Carbon::parse($entrega->dataSaida)->format('d/m/Y') }}</td>
                    <td>{{ Carbon\Carbon::parse($entrega->dataEntrega)->format('d/m/Y') }}</td>
                    <td>{{ isset($entrega->motorista->nome) ? $entrega->motorista->nome : 'Produto não informado' }}</td>
                    <td>{{ isset($entrega->endereco->cep) ? $entrega->endereco->cep : 'Endereço não informado' }}
                        <button class="btn btn-outline-primary btn-sm button_right" data-toggle="modal" data-target="#modal{{ $entrega->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button></td>
                    <td>
                        <a href="{{ route('entregas.edit', ['id' => $entrega->id]) }}"
                            class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{ $entrega->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>


                <!-- Modal -->
                <div class="modal fade" id="modal{{ $entrega->id }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel"><b> Visualizando Endereço: {{ $entrega->endereco->cep }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $entrega->endereco->id }} </p>
                                <p> <b>Logradouro: </b> {{ $entrega->endereco->logradouro }} </p>
                                <p> <b>Número: </b> {{ $entrega->endereco->numero }} </p>
                                <p> <b>Bairro: </b> {{ $entrega->endereco->bairro }} </p>
                                <p> <b>Cidade: </b> {{ $entrega->endereco->cidade }} </p>
                                <p> <b>Estado: </b> {{ $entrega->endereco->estado }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <!-- Modal
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Visualizar CEP</h4>
                    </div>
                    <form action="{{ route('enderecos.store', 'test') }}" method="post">
                        {{ method_field('patch') }}
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <input type="hidden" name="category_id" id="cat_id" value="">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title">
                            </div>
                            <div class="form-group">
                                <label for="des">Description</label>
                                <textarea name="description" id="des" cols="20" rows="5" id='des' class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->



    {{ $entregas->links('pagination::bootstrap-4') }}

    <a href="{{ route('entregas.create', []) }}" class="btn btn-primary">Adicionar</a>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" crossorigin="anonymous">
    </script>
    <script src="/js/scripts.js"></script>

@stop

@section('table-delete')
    "entregas"
@endsection
