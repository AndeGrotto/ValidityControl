<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lotes</title>
    <style>
        #lote{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #lote td,#lote th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #lote tr:nth-child(even){
            background-color: #c9eeee;
        }
        #lote th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="lote">
        <thead>
            <th>ID</th>
            <th>Quantidade de Produto</th>
            <th>Data de Fabricação</th>
            <th>Data de Validade</th>
            <th>Produto*</th>
        </thead>
        <tbody>
            @foreach ($lotes as $lote)
                <tr>
                    <td>{{ $lote->id }}</td>
                    <td>{{ $lote->quantidadeProduto }}</td>
                    <td>{{ Carbon\Carbon::parse($lote->dataFabricacao)->format('d/m/Y') }}</td>
                    <td>{{ Carbon\Carbon::parse($lote->dataValidade)->format('d/m/Y') }}</td>
                    <td>{{ isset($lote->produto->descricao) ? $lote->produto->descricao : 'Produto não informado' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>