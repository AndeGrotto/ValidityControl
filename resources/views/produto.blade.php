<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Produtos</title>
    <style>
        #produto{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #produto td,#produto th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #produto tr:nth-child(even){
            background-color: #c9eeee;
        }
        #produto th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="produto">
        <thead>
            <th>ID</th>
            <th>Estoque</th>
            <th>Descrição</th>
            <th>Validade</th>
            <th>Preço</th>
            <th>Tipo de Bebida</th>
        </thead>
        <tbody>
            @foreach ($produtos as $produto)
                <tr>
                    <td>{{ $produto->id }}</td>
                    <td>{{ $produto->estoque }}</td>
                    <td>{{ $produto->descricao }}</td>
                    <td>{{ $produto->validade }}</td>
                    <td>R$ {{ $produto->preco }}</td>
                    <td>{{ $produto->tipoBebida }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>