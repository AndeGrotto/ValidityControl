<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Motoristas</title>
    <style>
        #motorista{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #motorista td,#motorista th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #motorista tr:nth-child(even){
            background-color: #c9eeee;
        }
        #motorista th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="motorista">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Caminhão*</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($motoristas as $motorista)
                <tr>
                    <td>{{ $motorista->id }}</td>
                    <td>{{ $motorista->nome }}</td>
                    <td>{{ $motorista->telefone }}</td>
                    <td>{{ isset($motorista->caminhao->placa) ? $motorista->caminhao->placa : "Caminhão não informado" }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>