<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientes</title>
    <style>
        #cliente{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #cliente td,#cliente th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #cliente tr:nth-child(even){
            background-color: #c9eeee;
        }
        #cliente th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="cliente">
        <thead>
            <tr>
                <th>ID</th>
                <th>Razão Social</th>
                <th>CPF/CNPJ</th>
                <th>Situação</th>
                <th>Telefone</th>
                <th>Data de Cadastro</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->id }}</td>
                    <td>{{ $cliente->razaoSocial }}</td>
                    <td>{{ $cliente->cpf_cnpj }}</td>
                    <td>{{ $cliente->situacao }}</td>
                    <td>{{ $cliente->telefone }}</td>
                    <td>{{ Carbon\Carbon::parse($cliente->dataCadastro)->format('d/m/Y') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>