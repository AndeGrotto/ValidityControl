<link rel="stylesheet" href="/css/style.css">
@extends('layouts.default')

@section('content')

    <h1>Pedidos</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'pedidos']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}

    <br>

    <a href="{{ route('pedidos.exportPedidosPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('pedidos.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Data do Pedido</th>
            <th>Valor Total</th>
            <th>Cliente*</th>
            <th>Produto(s)*</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($pedidos as $pedido)
                <tr>
                    <td>{{ $pedido->id }}</td>
                    <td>{{ Carbon\Carbon::parse($pedido->dataPedido)->format('d/m/Y') }}</td>
                    <td>R$ {{ $pedido->valorTotal }}</td>
                    <td>{{ isset($pedido->cliente->razaoSocial) ? $pedido->cliente->razaoSocial : 'Cliente não informado' }}<button
                            class="btn btn-outline-primary btn-sm button_right" data-toggle="modal"
                            data-target="#modal1{{ $pedido->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </td>
                    <td>
                        <button class="btn btn-outline-primary btn-sm button_master" data-toggle="modal"
                            data-target="#modal2{{ $pedido->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        @foreach ($pedido->produtos as $p)
                            <li>{{ isset($p->produto->descricao) ? $p->produto->descricao : 'Produto não informado' }}</li>
                        @endforeach

                    </td>
                    <td>
                        <a href="{{ route('pedidos.edit', ['id' => $pedido->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{ $pedido->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>

                <!-- Modal Cliente -->
                <div class="modal fade" id="modal1{{ $pedido->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel1"><b> Visualizando Cliente:
                                        {{ $pedido->cliente->nome }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $pedido->cliente->id }} </p>
                                <p> <b>Razão Social: </b> {{ $pedido->cliente->razaoSocial }} </p>
                                <p> <b>CPF/CNPJ: </b> {{ $pedido->cliente->cpf_cnpj }} </p>
                                <p> <b>Situação: </b> {{ $pedido->cliente->situacao }} </p>
                                <p> <b>Telefone: </b> {{ $pedido->cliente->telefone }} </p>
                                <p> <b>Data de Cadastro: </b>
                                    {{ Carbon\Carbon::parse($pedido->cliente->dataCadastro)->format('d/m/Y') }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Produtos -->
                <div class="modal fade" id="modal2{{ $pedido->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel2"><b> Visualizando Produtos</b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            @foreach ($pedido->produtos as $p)
                                                <p> <b>ID: </b> {{ $p->produto->id }} </p>
                                                <p> <b>Estoque: </b> {{ $p->produto->estoque }} </p>
                                                <p> <b>Descrição: </b> {{ $p->produto->descricao }} </p>
                                                <p> <b>Validade: </b> {{ $p->produto->validade }} </p>
                                                <p> <b>Preço: </b> {{ $p->produto->preco }} </p>
                                                <p> <b>Tipo de Bebida: </b> {{ $p->produto->tipoBebida }} </p>
                                                <hr>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    {{ $pedidos->links('pagination::bootstrap-4') }}

    <a href="{{ route('pedidos.create', []) }}" class="btn btn-primary">Adicionar</a>


@stop

@section('table-delete')
    "pedidos"
@endsection
