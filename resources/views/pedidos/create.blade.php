<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Pedido </h3>
            </div>
            <div class="icon">
                <i class="fa fa-user-tie" aria-hidden="true"></i>
            </div>
        </div>

        @if (session('erro'))
            <div class="alert alert-danger" role="alert">
                {{ session('erro') }}
            </div>
        @endif

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'pedidos.store']) !!}

            <div class="form-group">
                {!! Form::label('dataPedido', 'Data do Pedido:') !!}
                {!! Form::date('dataPedido', null, ['class' => 'form-control', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('cliente_id', 'Cliente:') !!}
                {!! Form::select(
                    'cliente_id',
                    \App\Models\Cliente::orderBy('razaoSocial')->pluck('razaoSocial', 'id')->toArray(),
                    null,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>

            <hr />

            <h4>Produtos</h4>
            <div class="input_fields_wrap"></div>
            <br>

            <button style="float: right" class="add_field_button btn btn-info">Adicionar Produto</button>

            <br>
            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('pedidos', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@stop

@section('js')
    <script>
        $(document).ready(function() {
            var wrapper = $(".input_fields_wrap");
            var add_button = $(".add_field_button");
            var x = 0;
            $(add_button).click(function(e) {
                x++;
                var newField =
                    '<div><div style="width:80%; float:left" id="produto"> {!! Form::select(
                        'produtos[]',
                        \App\Models\Produto::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                        null,
                        ['class' => 'form-control', 'required', 'placeholder' => 'Selecione um produto'],
                    ) !!} </div><div style="width:10%; float:left">{!! Form::number('quantidadeItem[]', null, ['class' => 'form-control quantidadeItem', 'required', 'placeholder' => 'Quantidade']) !!}</div><button type="button" class="remove_field btn btn-danger btn-circle"><i class="fa fa-times"></button></div>';
                $(wrapper).append(newField);
            });
            $(wrapper).on("click", ".remove_field", function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>

<script type="text/javascript" src="/js/scripts.js"></script>

@stop
