<link rel="stylesheet" href="/css/style.css">
@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3>Editando Pedido: {{ Carbon\Carbon::parse($pedido->dataPedido)->format('d/m/Y') }}</h3>
            </div>
            <div class="icon">
                <i class="fa fa-user-tie" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => ['pedidos.update', 'id' => $pedido->id], 'method' => 'put']) !!}

            <div class="form-group">
                {!! Form::label('dataPedido', 'Data do Pedido:') !!}
                {!! Form::date('dataPedido', $pedido->dataPedido, ['class' => 'form-control', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('cliente_id', 'Cliente:') !!}
                {!! Form::select(
                    'cliente_id',
                    \App\Models\Cliente::orderBy('razaoSocial')->pluck('razaoSocial', 'id')->toArray(),
                    $pedido->cliente_id,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>

            <hr />

            <h4>Produtos</h4>
            <div class="input_fields_wrap"></div>
            <br>

            <button style="float: right" class="add_field_button btn btn-info">Adicionar Produto</button>
            <div>
                @foreach ($pedido->produtos as $p)
                    <div style="width:70%; float:left"> {!! Form::select(
                        'produto[]',
                        \App\Models\Produto::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                        $pedido->produto_id,
                        ['class' => 'form-control', 'required'],
                    ) !!}
                    </div><button type="button" class="remove_field btn btn-danger btn-circle"><i
                            class="fa fa-times"></i></button>
                    <br>
                @endforeach

                <hr />
            </div>




            <div class="form-group">
                {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('pedidos', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            var wrapper = $(".input_fields_wrap");
            var add_button = $(".add_field_button");
            var x = 0;
            $(add_button).click(function(e) {
                x++;
                var newField =
                    '<div><div style="width:70%; float:left"> {!! Form::select(
                        'produto[]',
                        \App\Models\Produto::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                        $pedido->produto_id,
                        ['class' => 'form-control', 'required', 'placeholder' => 'Selecione um produto'],
                    ) !!} </div><button type="button" class="remove_field btn btn-danger btn-circle"><i class="fa fa-times"></i></button></div>';
                $(wrapper).append(newField);
            });
            $(wrapper).on("click", ".remove_field", function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
        })
    </script>

@stop
