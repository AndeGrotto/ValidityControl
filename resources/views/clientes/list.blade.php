<link rel="stylesheet" href="/css/style.css">

@extends('layouts.default')

@section('content')

    <h1>Clientes</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'clientes']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}

    <br>

    <a href="{{ route('clientes.exportClientesPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('clientes.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Razão Social</th>
            <th>CPF/CNPJ</th>
            <th>Situação</th>
            <th>Telefone</th>
            <th>Data de Cadastro</th>
            <th>Endereço*</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->id }}</td>
                    <td>{{ $cliente->razaoSocial }}</td>
                    <td>{{ $cliente->cpf_cnpj }}</td>
                    <td>{{ $cliente->situacao }}</td>
                    <td>{{ $cliente->telefone }}</td>
                    <td>{{ Carbon\Carbon::parse($cliente->dataCadastro)->format('d/m/Y') }}</td>
                    <td>{{ isset($cliente->endereco->cep) ? $cliente->endereco->cep : 'Endereço não informado' }}<button
                            class="btn btn-outline-primary btn-sm button_right" data-toggle="modal"
                            data-target="#modal{{ $cliente->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </td>
                    <td>
                        <a href="{{ route('clientes.edit', ['id' => $cliente->id]) }}"
                            class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{ $cliente->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>

                <!-- Modal -->
                <div class="modal fade" id="modal{{ $cliente->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel"><b> Visualizando Endereço:
                                        {{ $cliente->endereco->cep }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $cliente->endereco->id }} </p>
                                <p> <b>Logradouro: </b> {{ $cliente->endereco->logradouro }} </p>
                                <p> <b>Número: </b> {{ $cliente->endereco->numero }} </p>
                                <p> <b>Bairro: </b> {{ $cliente->endereco->bairro }} </p>
                                <p> <b>Cidade: </b> {{ $cliente->endereco->cidade }} </p>
                                <p> <b>Estado: </b> {{ $cliente->endereco->estado }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    {{ $clientes->links('pagination::bootstrap-4') }}

    <a href="{{ route('clientes.create', []) }}" class="btn btn-primary">Adicionar</a>

@stop

@section('table-delete')
    "clientes"
@endsection
