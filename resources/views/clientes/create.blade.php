<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Cliente </h3>
            </div>
            <div class="icon">
                <i class="fa fa-user" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'clientes.store']) !!}

            <div class="form-group">
                {!! Form::label('razaoSocial', 'Razão Social:') !!}
                {!! Form::text('razaoSocial', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cpf_cnpj', 'CPF/CNPJ:') !!}
                {!! Form::text('cpf_cnpj', null, ['class' => 'form-control cpf_cnpj', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('situacao', 'Situação:') !!}
                {!! Form::select(
                    'situacao',
                    ['Ativo' => 'Ativo(a)', 'Pendente' => 'Pendente', 'Cancelado' => 'Cancelado(a)'],
                    'Ativo',
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>
            <div class="form-group">
                {!! Form::label('telefone', 'Telefone:') !!}
                {!! Form::text('telefone', null, ['class' => 'form-control telefone', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('dataCadastro', 'Data de Cadastro:') !!}
                {!! Form::date('dataCadastro', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('endereco_id', 'Endereço:') !!}
                {!! Form::select(
                    'endereco_id',
                    \App\Models\Endereco::orderBy('cidade')->pluck('cidade', 'id')->toArray(),
                    null,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>

            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('clientes', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <script src="/js/scripts.js"></script>

@stop
