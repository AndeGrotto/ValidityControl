<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Endereços</title>
    <style>
        #endereco{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #endereco td,#endereco th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #endereco tr:nth-child(even){
            background-color: #c9eeee;
        }
        #endereco th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="endereco">
        <thead>
            <th>ID</th>
            <th>CEP</th>
            <th>Logradouro</th>
            <th>Número</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>Estado</th>
            <th>Data de criação</th>
        </thead>
        <tbody>
            @foreach ($enderecos as $endereco)
                <tr>
                    <td>{{ $endereco->id }}</td>
                    <td>{{ $endereco->cep }}</td>
                    <td>{{ $endereco->logradouro }}</td>
                    <td>{{ $endereco->numero }}</td>
                    <td>{{ $endereco->bairro }}</td>
                    <td>{{ $endereco->cidade }}</td>
                    <td>{{ $endereco->estado }}</td>
                    <td>{{ (new DateTime($endereco->created_at))->format('d/m/Y H:i:s') }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>