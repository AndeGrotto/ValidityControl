<link rel="stylesheet" href="/css/style.css">
@extends('adminlte::page')

@section('content')

    <div class="card">
            <div class="card-box card-header bg-blue">
                <div class="inner">
                    <h3> Novo Usuário </h3>
                </div>
                <div class="icon">
                    <i class="fa fa-user-tie" aria-hidden="true"></i>
                </div>
            </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'users.store']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Nome:') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'E-mail:') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', 'Senha:') !!}
                {!! Form::text('password', null, ['class' => 'form-control', 'required']) !!}
            </div>

            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('users', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@stop
