<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Motorista </h3>
            </div>
            <div class="icon">
                <i class="fa fa-id-card" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'motoristas.store']) !!}

            <div class="form-group">
                {!! Form::label('nome', 'Nome:') !!}
                {!! Form::text('nome', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('telefone', 'Telefone:') !!}
                {!! Form::text('telefone', null, ['class' => 'form-control', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('caminhao_id', 'Caminhao:') !!}
                {!! Form::select(
                    'caminhao_id',
                    \App\Models\Caminhao::orderBy('placa')->pluck('placa', 'id')->toArray(),
                    null,
                    ['class' => 'form-control', 'required'],
                ) !!}
            </div>

            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('motoristas', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <script type="text/javascript" src="/js/scripts.js"></script>

@stop
