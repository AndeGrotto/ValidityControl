<link rel="stylesheet" href="/css/style.css">

@extends('layouts.default')

@section('content')

    <h1>Motoristas</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'motoristas']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}

    <br>

    <a href="{{ route('motoristas.exportMotoristasPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('motoristas.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Caminhão*</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($motoristas as $motorista)
                <tr>
                    <td>{{ $motorista->id }}</td>
                    <td>{{ $motorista->nome }}</td>
                    <td>{{ $motorista->telefone }}</td>
                    <td>{{ isset($motorista->caminhao->placa) ? $motorista->caminhao->placa : 'Caminhão não informado' }}<button class="btn btn-outline-primary btn-sm button_right" data-toggle="modal" data-target="#modal{{ $motorista->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </td>
                    <td>
                        <a href="{{ route('motoristas.edit', ['id' => $motorista->id]) }}"
                            class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{ $motorista->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>

                <!-- Modal -->
                <div class="modal fade" id="modal{{ $motorista->id }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel"><b> Visualizando Caminhão: {{ $motorista->caminhao->placa }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $motorista->caminhao->id }} </p>
                                <p> <b>Placa: </b> {{ $motorista->caminhao->placa }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    {{ $motoristas->links('pagination::bootstrap-4') }}

    <a href="{{ route('motoristas.create', []) }}" class="btn btn-primary">Adicionar</a>

@stop

@section('table-delete')
    "motoristas"
@endsection
