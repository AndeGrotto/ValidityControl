<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Itens Pedido</title>
    <style>
        #itensPedido{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #itensPedido td,#itensPedido th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #itensPedido tr:nth-child(even){
            background-color: #c9eeee;
        }
        #itensPedido th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="itensPedido">
        <thead>
            <th>ID</th>
            <th>Produto</th>
            <th>Pedido</th>
            <th>Quantidade de Itens</th>
            <th>Valor Total</th>
        </thead>
        <tbody>
            @foreach ($itensPedidos as $itensPedido)
                <tr>
                    <td>{{ $itensPedido->id }}</td>
                    <td>{{ isset($itensPedido->produto->descricao) ? $itensPedido->produto->descricao : "Produto não informado" }}</td>
                    <td>{{ isset($itensPedido->pedido->dataPedido) ? $itensPedido->pedido->dataPedido : "Pedido não informado" }}</td>
                    <td>{{ $itensPedido->quantidadeItem }}</td>
                    <td>R$ {{ $itensPedido->valorTotalItem }}</td>
                </tr>
            @endforeach
            </tbody>
    </table>
</body>
</html>