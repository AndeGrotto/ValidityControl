<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')

    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Produto </h3>
            </div>
            <div class="icon">
                <i class="fa fa-box" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            {!! Form::open(['route' => 'produtos.store']) !!}

            <div class="form-group">
                {!! Form::label('estoque', 'Estoque:') !!}
                {!! Form::number('estoque', 0, ['class' => 'form-control', 'readonly']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('descricao', 'Descrição:') !!}
                {!! Form::text('descricao', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('validade', 'Validade:') !!}
                {!! Form::number('validade', null, ['class' => 'form-control', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('preco', 'Preço:') !!}
                {!! Form::text('preco', null, ['class' => 'form-control', 'required', 'step' => 'any']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('tipoBebida', 'Tipo de Bebida:') !!}
                {!! Form::select('tipoBebida', ['Refrigerante' => 'Refrigerante', 'Água' => 'Água', 'Suco' => 'Suco', 'Outro' => 'Outro',], null, [
                    'class' => 'form-control',
                    'required',
                ]) !!}
            </div>

            <hr />

            <div class="form-group">
                {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                <a href="{{ route('produtos', []) }}" class="btn btn-secondary">Voltar</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <script src="/js/scripts.js"></script>

@stop
