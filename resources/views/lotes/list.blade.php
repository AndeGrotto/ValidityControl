<link rel="stylesheet" href="/css/style.css">

@extends('layouts.default')

@section('content')

    <h1>Lotes</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'lotes']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}

    <br>

    <a href="{{ route('lotes.exportLotesPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('lotes.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Quantidade de Produto</th>
            <th>Data de Fabricação</th>
            <th>Data de Validade</th>
            <th>Produto*</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($lotes as $lote)
                <tr>
                    <td>{{ $lote->id }}</td>
                    <td>{{ $lote->quantidadeProduto }}</td>
                    <td>{{ Carbon\Carbon::parse($lote->dataFabricacao)->format('d/m/Y') }}</td>
                    <td>{{ Carbon\Carbon::parse($lote->dataValidade)->format('d/m/Y') }}</td>
                    <td>{{ isset($lote->produto->descricao) ? $lote->produto->descricao : 'Produto não informado' }}<button
                            class="btn btn-outline-primary btn-sm button_right" data-toggle="modal"
                            data-target="#modal{{ $lote->id }}"><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </td>
                    <td>
                        <a href="{{ route('lotes.edit', ['id' => $lote->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmaExclusao({{ $lote->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>

                <!-- Modal -->
                <div class="modal fade" id="modal{{ $lote->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel"><b> Visualizando Produto:
                                        {{ $lote->produto->descricao }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $lote->produto->id }} </p>
                                <p> <b>Estoque: </b> {{ $lote->produto->estoque }} </p>
                                <p> <b>Descrição: </b> {{ $lote->produto->descricao }} </p>
                                <p> <b>Validade: </b> {{ $lote->produto->validade }} </p>
                                <p> <b>Preço: </b> {{ $lote->produto->preco }} </p>
                                <p> <b>Tipo de bebida: </b> {{ $lote->produto->tipoBebida }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    {{ $lotes->links('pagination::bootstrap-4') }}

    <a href="{{ route('lotes.create', []) }}" class="btn btn-primary">Adicionar</a>
@stop

@section('table-delete')
    "lotes"
@endsection
