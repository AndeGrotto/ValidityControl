@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4 style="padding: 15px; text-align: center">Bem Vindo(a), {{ Auth::user()->name }}!</h4>    
                    <br>
                
                <div class="row-cols-1">
                    <div class="small-box bg-gradient-blue">
                        <div class="inner">
                        <h3>{{ $pedidos }}</h3>
                        <p>Pedidos</p>
                        </div>
                        <div class="icon">
                        <i class="ion ion-bag"></i>
                        </div>
                        <a href="http://127.0.0.1:8000/pedidos" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                        </div>

                        <div class="small-box bg-gradient-navy">
                            <div class="inner">
                            <h3>{{ $produtos }}</h3>
                            <p>Produtos</p>
                            </div>
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                            </div>
                            <a href="http://127.0.0.1:8000/produtos" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                            </div>        
                </div>    
                        <div class="row">
                            <div class="col-lg-3 col-6">
                            
                            <div class="small-box bg-gradient-indigo">
                            <div class="inner">
                            <h3>{{ $clientes }}</h3>
                            <p>Clientes</p>
                            </div>
                            <div class="icon">
                            <i class="ion ion-bag"></i>
                            </div>
                            <a href="http://127.0.0.1:8000/clientes" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                            </div>
                            
                            <div class="col-lg-3 col-6">
                            
                            <div class="small-box bg-gradient-success">
                            <div class="inner">
                            <h3>{{ $enderecos }}</h3>
                            <p>Enderecos</p>
                            </div>
                            <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="http://127.0.0.1:8000/enderecos" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                            </div>
                            
                            <div class="col-lg-3 col-6">
                            
                            <div class="small-box bg-gradient-dark">
                            <div class="inner">
                            <h3>{{ $entregas }}</h3>
                            <p>Entregas</p>
                            </div>
                            <div class="icon">
                            <i class="ion ion-person-add"></i>
                            </div>
                            <a href="http://127.0.0.1:8000/entregas" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                            </div>
                            
                            <div class="col-lg-3 col-6">
                            
                            <div class="small-box bg-gradient-danger">
                            <div class="inner">
                            <h3>{{ $caminhoes }}</h3>
                            <p>Caminhões</p>
                            </div>
                            <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="http://127.0.0.1:8000/pedidos" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                            </div>
                            
                            </div>  
                            <div class="row">
                                <div class="col-lg-3 col-6">
                                
                                <div class="small-box bg-gradient-indigo">
                                <div class="inner">
                                <h3>{{ $itensPedidos }}</h3>
                                <p>Itens de Pedidos</p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-bag"></i>
                                </div>
                                <a href="http://127.0.0.1:8000/itensPedidos" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                                </div>
                                
                                <div class="col-lg-3 col-6">
                                
                                <div class="small-box bg-gradient-success">
                                <div class="inner">
                                <h3>{{ $lotes }}</h3>
                                <p>Lotes</p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="http://127.0.0.1:8000/lotes" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                                </div>
                                
                                <div class="col-lg-3 col-6">
                                
                                <div class="small-box bg-gradient-dark">
                                <div class="inner">
                                <h3>{{ $motoristas }}</h3>
                                <p>Motoristas</p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-person-add"></i>
                                </div>
                                <a href="http://127.0.0.1:8000/motoristas" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                                </div>
                                
                                <div class="col-lg-3 col-6">
                                
                                <div class="small-box bg-gradient-danger">
                                <div class="inner">
                                <h3>{{ $users }}</h3>
                                <p>Usuários</p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="http://127.0.0.1:8000/users" class="small-box-footer">Ver mais <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                                </div>
                                
                                </div>                            
                            


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
