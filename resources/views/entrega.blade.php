<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entregas</title>
    <style>
        #entrega{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #entrega td,#entrega th{
            border: 1px solid rgb(172, 172, 172);
            padding: 8px;
        }
        #entrega tr:nth-child(even){
            background-color: #c9eeee;
        }
        #entrega th{
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3967fffd;
            color:rgb(255, 255, 255);
        }
        
    </style>

</head>
<body>
    <table id="entrega">
        <thead>
            <th>ID</th>
            <th>Data de Saída</th>
            <th>Data de Entrega</th>
            <th>Motorista*</th>
            <th>Endereço*</th>
        </thead>
        <tbody>
            @foreach ($entregas as $entrega)
                <tr>
                    <td>{{ $entrega->id }}</td>
                    <td>{{ Carbon\Carbon::parse($entrega->dataSaida)->format('d/m/Y') }}</td>
                    <td>{{ Carbon\Carbon::parse($entrega->dataEntrega)->format('d/m/Y') }}</td>
                    <td>{{ isset($entrega->motorista->nome) ? $entrega->motorista->nome : 'Produto não informado' }}</td>
                    <td>{{ isset($entrega->endereco->cep) ? $entrega->endereco->cep : 'Endereço não informado' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>