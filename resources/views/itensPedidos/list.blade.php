<link rel="stylesheet" href="/css/style.css">

@extends('layouts.default')

@section('content')
    <h1>Itens Pedidos</h1>

    {!! Form::open(['name' => 'form_name', 'route' => 'itensPedidos']) !!}
    <div calss="sidebar-form">
        <div class="input-group">
            <input type="text" name="desc_filtro" class="form-control" style="width:80% !important;"
                placeholder="Pesquisa...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-default"><i
                        class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
    {!! Form::close() !!}
    <br>

    <a href="{{ route('itensPedidos.exportItensPedidosPDF', []) }}" class="btn btn-warning">Exportar PDF</a>
    <a href="{{ route('itensPedidos.export', []) }}" class="btn btn-success">Exportar Excel</a>

    <hr>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Pedido*</th>
            <th>Produto*</th>
            <th>Quantidade de Itens</th>
            <th>Valor Total</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($itensPedidos as $itensPedido)
                <tr>
                    <td>{{ $itensPedido->id }}</td>
                    <td>{{ Carbon\Carbon::parse($itensPedido->pedido->dataPedido)->format('d/m/Y') }}
                        <button
                             class="btn btn-outline-primary btn-sm button_right" data-toggle="modal"
                             data-target="#modal2{{ $itensPedido->id }}"><i class="fa fa-eye"
                                 aria-hidden="true"></i></button>
                     </td>
                    <td>{{ isset($itensPedido->produto->descricao) ? $itensPedido->produto->descricao : 'Produto não informado' }}<button
                            class="btn btn-outline-primary btn-sm button_right" data-toggle="modal"
                            data-target="#modal1{{ $itensPedido->id }}"><i class="fa fa-eye"
                                aria-hidden="true"></i></button>
                    </td>
                    <td>{{ $itensPedido->quantidadeItem }}</td>
                    <td>R$ {{ $itensPedido->valorTotalItem }}</td>
                    <td>
                        <!--<a href="{{ route('itensPedidos.edit', ['id' => $itensPedido->id]) }}"
                            class="btn-sm btn-success">Editar</a>-->
                        <a href="#" onclick="return ConfirmaExclusao({{ $itensPedido->id }})"
                            class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>

                <!-- Modal Produto -->
                <div class="modal fade" id="modal1{{ $itensPedido->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel1"><b> Visualizando Produto:
                                        {{ $itensPedido->produto->descricao }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $itensPedido->produto->id }} </p>
                                <p> <b>Estoque: </b> {{ $itensPedido->produto->estoque }} </p>
                                <p> <b>Descrição: </b> {{ $itensPedido->produto->descricao }} </p>
                                <p> <b>Validade: </b> {{ $itensPedido->produto->validade }} </p>
                                <p> <b>Preço: </b> {{ $itensPedido->produto->preco }} </p>
                                <p> <b>Tipo de Bebida: </b> {{ $itensPedido->produto->tipoBebida }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Pedido-->
                <div class="modal fade" id="modal2{{ $itensPedido->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalLabel2"><b> Visualizando Pedido:
                                        {{ Carbon\Carbon::parse($itensPedido->pedido->dataPedido)->format('d/m/Y') }} </b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p> <b>ID: </b> {{ $itensPedido->pedido->id }} </p>
                                <p> <b>Data do Pedido: </b>
                                    {{ Carbon\Carbon::parse($itensPedido->pedido->dataPedido)->format('d/m/Y') }} </p>
                                <p> <b>Valor Total: </b> R$ {{ $itensPedido->pedido->valorTotal }} </p>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    {{ $itensPedidos->links('pagination::bootstrap-4') }}

    <!-- <a href="{{ route('itensPedidos.create', []) }}" class="btn btn-primary">Adicionar</a> -->
@stop

@section('table-delete')
    "itensPedidos"
@endsection
