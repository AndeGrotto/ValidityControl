<link rel="stylesheet" href="/css/style.css">
@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Item do Pedido </h3>
            </div>
            <div class="icon">
                <i class="fa fa-cart-plus" aria-hidden="true"></i>
            </div>
        </div>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="card-body">

    {!! Form::open(['route'=>'itensPedidos.store']) !!}

    <div class="form-group">
        {!! Form::label('produto_id', '*Produto:') !!}
        {!! Form::select('produto_id', 
                        \App\Models\Produto::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                        null, ['class'=>'form-control', 'required'])!!}            
    </div>
    <div class="form-group">
        {!! Form::label('pedido_id', '*Pedido:') !!}
        {!! Form::select('pedido_id', 
                        \App\Models\Pedido::orderBy('dataPedido')->pluck('dataPedido', 'id')->toArray(),
                        null, ['class'=>'form-control', 'required'])!!}            
    </div>
    <div class="form-group">
        {!! Form::label('quantidadeItem', 'Quantidade:') !!}
        {!! Form::number('quantidadeItem', null, ['class'=>'form-control', 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('valorTotal', 'Valor Total:') !!}
        {!! Form::number('valorTotal', null, ['class'=>'form-control', 'required', 'step'=>'any']) !!}
    </div>

    <hr />

    <div class="form-group">
        {!! Form::submit('Cadastrar', ['class'=>'btn btn-primary']) !!}
        {!! Form::reset('Limpar', ['class'=>'btn btn-danger']) !!}
        <a href="{{ route('itensPedidos', []) }}" class="btn btn-secondary">Voltar</a>
    </div>


    {!! Form::close() !!}
</div>
</div>
@stop