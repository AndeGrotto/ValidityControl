<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-header" style="background: lightgray">
            <h3 style="font-weight: bold;">Buscar CEP</h3>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        @if (session('sucesso'))
            <div class="alert alert-success" role="alert">
                {{ session('sucesso') }}
            </div>
        @endif
        @if (session('erro'))
            <div class="alert alert-danger" role="alert">
                {{ session('erro') }}
            </div>
        @endif

        <div class="card-body">

            <form action="{{ route('enderecos.search_cep') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label>Informe o CEP:</label>
                    <input type="text" class="form-control cep" id="cep" name="cep">
                </div>

                <hr />

                <div class="form-group">
                    {!! Form::submit('Buscar', ['class' => 'btn btn-primary']) !!}
                    {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route('enderecos', []) }}" class="btn btn-secondary">Voltar</a>
                </div>
                <br>
                <p class="hipertext">
                    <a href="https://achacep.com.br/" target="_blank">Não sabe o seu CEP? Clique aqui!</a>
                </p>
            </form>
            <script type="text/javascript" src="/js/scripts.js"></script>
        </div>
    </div>

@stop
