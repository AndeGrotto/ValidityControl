<link rel="stylesheet" href="/css/style.css">

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-box card-header bg-blue">
            <div class="inner">
                <h3> Novo Endereço </h3>
            </div>
            <div class="icon">
                <i class="fa fa-map-pin" aria-hidden="true"></i>
            </div>
        </div>

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="card-body">

            <form action="{{ route('enderecos.store') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label>CEP:</label>
                    <input type="text" class="form-control cep" id="cep" name="cep" value="{{ $cep }}" readonly>
                </div>
                <div class="mb-3">
                    <label>Logradouro:</label>
                    <input  type="text" class="form-control" name="logradouro" value="{{ $logradouro }}">
                </div>
                <div class="mb-3">
                    <label>Número:</label>
                    <input id="numero" type="number" class="form-control" name="numero">
                </div>
                <div class="mb-3">
                    <label>Bairro:</label>
                    <input type="text" class="form-control" name="bairro" value="{{ $bairro }}">
                </div>
                <div class="mb-3">
                    <label>Cidade:</label>
                    <input type="text" class="form-control" name="cidade" value="{{ $cidade }}" readonly>
                </div>
                <div class="mb-3">
                    <label>Estado:</label>
                    <input type="text" class="form-control uf" name="estado" value="{{ $estado }}" readonly>
                </div>

                <hr />

                <div class="form-group">
                    {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                    {!! Form::reset('Limpar', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route('enderecos', []) }}" class="btn btn-secondary">Voltar</a>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="/js/scripts.js"></script>

@stop
