# ValidityControl
<p align="center"><a href="#" target="_blank"><img src="http://marcusmarques.com.br/wp-content/uploads/2017/01/inventario.jpg" width="400"></a></p>



O sistema será utilizado para gerenciar a validade dos produtos de uma empresa, para evitar que os produtos fabricados acabem perdendo a validade e sejam descartados.

## Classes
- [Usuário](#usuario).
- [Cliente](#cliente).
- [Produto](#produto).
- [Lote](#lote).
- [Pedido](#pedido).
- [Itens do Pedido](#itensPedido).
- [Endereço](#endereco).
- [Entrega](#entrega).
- [Motorista](#motorista).
- [Caminhão](#caminhao).

### Usuário
- codigo.
- nome.
- email.
- senha.

### Cliente
- codigo.
- razaoSocial.
- cpf_cnpj.
- situacao.
- telefone.
- dataCadastro.

### Produto
- codigo.
- estoque.
- descricao.
- validade.
- preco.
- tipoBebida.

### Lote
- codigo.
- quantidadeProduto.
- dataFabricacao.
- dataValidade.

### Pedido
- codigo.
- dataPedido.
- valorTotal.

### Itens do Pedido
- quantidadeItem.
- valorTotalItem.

### Endereço
- codigo.
- cidade.
- estado.
- cep.
- bairro.
- rua.
- numero.

### Entrega
- codigo.
- dataSaida.
- dataEntrega.

### Motorista
- codigo.
- nome.
- telefone.

### Caminhão
- codigo.
- placa.