<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateItensPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itensPedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantidadeItem');
            $table->double('valorTotalItem');

            $table->bigInteger('produto_id')->unsigned()->nullable();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
            
            $table->bigInteger('pedido_id')->unsigned()->nullable();
            $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');
            $table->timestamps();
        });

        DB::unprepared('DROP TRIGGER IF EXISTS AtualizarEstoque_InsertPedido');
        DB::unprepared(
            'CREATE TRIGGER AtualizarEstoque_InsertPedido AFTER INSERT
            ON itenspedidos
            FOR EACH ROW
            BEGIN
                UPDATE produtos SET estoque = estoque - NEW.quantidadeItem
            WHERE id = NEW.produto_id;
            END'
        );

        DB::unprepared('DROP TRIGGER IF EXISTS AtualizarEstoque_DeletePedido');
        DB::unprepared(
            'CREATE TRIGGER AtualizarEstoque_DeletePedido AFTER DELETE
            ON itenspedidos
            FOR EACH ROW
            BEGIN
	            UPDATE produtos SET estoque = estoque + OLD.quantidadeItem
            WHERE id = OLD.produto_id;
            END'
        );

        /*DB::unprepared('DROP TRIGGER IF EXISTS AtualizarEstoque_UpdatePedido');
        DB::unprepared(
            'CREATE TRIGGER AtualizarEstoque_UpdatePedido AFTER UPDATE
            ON itenspedidos
            FOR EACH ROW
            BEGIN
	            UPDATE produtos SET estoque = (estoque - 500)
            WHERE id = NEW.produto_id;
            END'
        );*/



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itensPedidos');
    }
}
