<?php

namespace Database\Seeders;

use App\Models\Motorista;
use Illuminate\Database\Seeder;

class MotoristaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Motorista::create(['nome' => 'Armindo Marques', 'telefone' => '(52) 91547-8966', 'caminhao_id' => 2]);
        Motorista::create(['nome' => 'Joaquim Pereira', 'telefone' => '(62) 98745-4521', 'caminhao_id' => 1]);
        Motorista::create(['nome' => 'Carlos Magros', 'telefone' => '(54) 99605-8388', 'caminhao_id' => 5]);
        Motorista::create(['nome' => 'Marquinhos Silva', 'telefone' => '(45) 98455-9355', 'caminhao_id' => 4]);
        Motorista::create(['nome' => 'Vagner Timbras', 'telefone' => '(22) 97621-2999', 'caminhao_id' => 3]);
    }
}
