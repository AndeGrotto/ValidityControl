<?php

namespace Database\Seeders;

use App\Models\Entrega;
use Illuminate\Database\Seeder;

class EntregaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entrega::create(['dataSaida' => '2022-09-30', 'dataEntrega' => '2022-10-05', 'motorista_id' => 5, 'endereco_id' => 2]);
        Entrega::create(['dataSaida' => '2022-10-15', 'dataEntrega' => '2022-10-22', 'motorista_id' => 4, 'endereco_id' => 5]);
        Entrega::create(['dataSaida' => '2022-05-16', 'dataEntrega' => '2022-06-01', 'motorista_id' => 1, 'endereco_id' => 3]);
        Entrega::create(['dataSaida' => '2022-07-22', 'dataEntrega' => '2022-08-06', 'motorista_id' => 2, 'endereco_id' => 4]);
        Entrega::create(['dataSaida' => '2022-06-20', 'dataEntrega' => '2022-06-23', 'motorista_id' => 3, 'endereco_id' => 1]);
    }
}
