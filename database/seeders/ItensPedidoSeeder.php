<?php

namespace Database\Seeders;

use App\Models\ItensPedido;
use Illuminate\Database\Seeder;

class ItensPedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ItensPedido::create(['quantidadeItem' => 30, 'valorTotalItem' => 24.99, 'produto_id' => 5, 'pedido_id' => 3]);
        ItensPedido::create(['quantidadeItem' => 25, 'valorTotalItem' => 29.99, 'produto_id' => 4, 'pedido_id' => 3]);

        ItensPedido::create(['quantidadeItem' => 20, 'valorTotalItem' => 39.99, 'produto_id' => 2, 'pedido_id' => 4]);
        ItensPedido::create(['quantidadeItem' => 15, 'valorTotalItem' => 43.99, 'produto_id' => 1, 'pedido_id' => 4]);
        ItensPedido::create(['quantidadeItem' => 20, 'valorTotalItem' => 39.99, 'produto_id' => 5, 'pedido_id' => 4]);

        ItensPedido::create(['quantidadeItem' => 10, 'valorTotalItem' => 19.99, 'produto_id' => 1, 'pedido_id' => 1]);
        ItensPedido::create(['quantidadeItem' => 50, 'valorTotalItem' => 39.99, 'produto_id' => 2, 'pedido_id' => 1]);
        ItensPedido::create(['quantidadeItem' => 40, 'valorTotalItem' => 39.99, 'produto_id' => 2, 'pedido_id' => 1]);
        ItensPedido::create(['quantidadeItem' => 15, 'valorTotalItem' => 39.99, 'produto_id' => 4, 'pedido_id' => 1]);
        ItensPedido::create(['quantidadeItem' => 15, 'valorTotalItem' => 49.99, 'produto_id' => 5, 'pedido_id' => 1]);

        ItensPedido::create(['quantidadeItem' => 10, 'valorTotalItem' => 59.99, 'produto_id' => 4, 'pedido_id' => 2]);
        ItensPedido::create(['quantidadeItem' => 2, 'valorTotalItem' => 59.99, 'produto_id' => 1, 'pedido_id' => 2]);

        ItensPedido::create(['quantidadeItem' => 7, 'valorTotalItem' => 60.99, 'produto_id' => 1, 'pedido_id' => 5]);
    }
}
