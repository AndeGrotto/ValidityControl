<?php

namespace Database\Seeders;

use App\Models\Pedido;
use Illuminate\Database\Seeder;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pedido::create(['dataPedido' => '2022-09-30', 'valorTotal' => '2.508,00', 'cliente_id' => 4]);
        Pedido::create(['dataPedido' => '2022-10-15', 'valorTotal' => '1.208,00', 'cliente_id' => 1]);
        Pedido::create(['dataPedido' => '2022-10-10', 'valorTotal' => '3.455,00', 'cliente_id' => 2]);
        Pedido::create(['dataPedido' => '2021-12-01', 'valorTotal' => '410,00', 'cliente_id' => 3]);
        Pedido::create(['dataPedido' => '2022-07-28', 'valorTotal' => '998,00', 'cliente_id' => 5]);
    }
}
