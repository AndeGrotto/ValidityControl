<?php

namespace Database\Seeders;

use App\Models\Caminhao;
use Illuminate\Database\Seeder;

class CaminhaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Caminhao::create(['placa' => 'ABC-7A95']);
        Caminhao::create(['placa' => 'CDA-8745']);
        Caminhao::create(['placa' => 'XKY-5478']);
        Caminhao::create(['placa' => 'FXO-8K64']);
        Caminhao::create(['placa' => 'JKL-1128']);
    }
}
