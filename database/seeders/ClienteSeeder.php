<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create(['razaoSocial' => 'AJ Tecnologias', 'cpf_cnpj' => '90.605.983/0001-82', 'situacao' => 'Ativo', 'telefone' => '(53) 98451-4744', 'dataCadastro' => '2022-09-19', 'endereco_id' => 4]);
        Cliente::create(['razaoSocial' => 'Casa do Fut', 'cpf_cnpj' => '18.492.273/0001-66', 'situacao' => 'Ativo', 'telefone' => '(51) 98696-6048', 'dataCadastro' => '2022-10-15', 'endereco_id' => 5]);
        Cliente::create(['razaoSocial' => 'Barbearia do Marquinhos', 'cpf_cnpj' => '47.790.456/0001-66', 'situacao' => 'Ativo', 'telefone' => '(54) 99713-1822', 'dataCadastro' => '2022-08-29', 'endereco_id' => 1]);
        Cliente::create(['razaoSocial' => 'Sinuca do Santos', 'cpf_cnpj' => '22.994.446/0001-78', 'situacao' => 'Cancelado', 'telefone' => '(54) 98777-1112', 'dataCadastro' => '2022-01-12', 'endereco_id' => 3]);
        Cliente::create(['razaoSocial' => 'Bar do Careca', 'cpf_cnpj' => '33.212.889/0001-24', 'situacao' => 'Pendente', 'telefone' => '(54) 99713-2492', 'dataCadastro' => '2022-04-09', 'endereco_id' => 2]);
    }
}
